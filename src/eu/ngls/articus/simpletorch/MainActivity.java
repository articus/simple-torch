package eu.ngls.articus.simpletorch;

import android.app.Activity;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {
	private Camera camera;
	private ImageView lightbulb;

	private OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (camera == null) {
				try {
					camera = Camera.open();
				} catch (RuntimeException e) {
					Toast.makeText(MainActivity.this, e.getLocalizedMessage(),
							Toast.LENGTH_LONG).show();
				}
			}

			if (camera == null) {
				Toast.makeText(getApplicationContext(), R.string.no_camera,
						Toast.LENGTH_LONG).show();
				return;
			}

			Parameters parameters = camera.getParameters();
			if (parameters.getFlashMode().equals(Parameters.FLASH_MODE_TORCH)) {
				parameters.setFlashMode(Parameters.FLASH_MODE_OFF);
				lightbulb.setImageResource(R.drawable.ic_action_off);
				lightbulb.setContentDescription(getString(R.string.off));
				camera.setParameters(parameters);
				camera.stopPreview();
				camera.release();
				camera = null;
			} else {
				// required on some devices like samsung gt-i8190
				camera.startPreview();
				parameters.setFlashMode(Parameters.FLASH_MODE_TORCH);
				lightbulb.setImageResource(R.drawable.ic_action_on);
				lightbulb.setContentDescription(getString(R.string.on));
				camera.setParameters(parameters);
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		lightbulb = (ImageView) findViewById(R.id.lightbulb);
		lightbulb.setOnClickListener(onClickListener);
	}

	@Override
	protected void onDestroy() {
		if (camera != null)
			camera.release();
		super.onDestroy();
	}

}
